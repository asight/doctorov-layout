$(document).ready(function() {

	//Custom select style
	$('.header__city select').selectize();
	$('.search__region select').selectize({
		placeholder: 'Метро или район',
	});
	$('.date__select').selectize();

	// Tabs
	$('ul.tabs__caption').on('click', 'li:not(.active)', function() {
		$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
	});

	// All doctors and clinics tabs
	$('.all-doctors__header').on('click', '.header__link:not(.header__link--active)', function() {
		$(this)
			.addClass('header__link--active').siblings().removeClass('header__link--active')
			.closest('.all-doctors').find('.all-doctors__main').find('div').toggleClass('main--active');
	});

	// Radio Tabs
	$('.form .tabs__caption').on('click', 'label:not(.active)', function() {
		$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
	});

	// Popup
	$(document).on('click', '.fancy-close', function() {
		$.fancybox.close();
	});
	var mobiFullScreen = {
		minWidth: 300
	};
	$('.fancybox-inline').fancybox({
		afterLoad: function () {
			if ($(this.element).hasClass("mobile__map")) {
				$.extend(this, mobiFullScreen);
				$(".fancybox-overlay").addClass("mobile-map-overlay");
			}
		}
	});

	$(".fancybox-gallery").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});




	$(".map--iframe").fancybox({type: 'iframe'});

	// Datepicker
	flatpickr(".datepicker", {
		"locale": "ru"
	});

	//Slider
	var slider = $('.glide').glide({ 
		autoplay: false,
		animationDuration: 1000
	});


	//Spoilers
	$('a.spoiler__link').click(function(){
		$(this).parent().children('ul.spoiler__body').slideToggle('normal');
		$(this).parent().toggleClass('spoiler--open');
		return false;
	});

	// Form part toggle
	$(document).on('click', '.form .form__btn-next, .form .form__change', function(event) {
		event.preventDefault();
		$(this).toggleClass('hidden-element').siblings().toggleClass('hidden-element');
	});

	// Scroll to Reviews section
	$(document).on('click', 'a.rating-stars__go-to-reviews', function(event) {
		event.preventDefault();
		var top = $('#reviews').offset().top - 70;
		$('body,html').animate({scrollTop: top}, 1000);
	});

	//Replace sidebar on gadgets
	var width = document.documentElement.clientWidth;
	var height = document.documentElement.clientHeight;
	if (width <= 982) {
		$('.single-doctors .description__doctor').after($('.single-doctors .doctors__sidebar'));
		$('.single-clinics .description .description__rating-stars').after($('.single-clinics .description .sidebar'));
	}
	else{
		$('.single-doctors .description>.container>.row:first-child>.col-9')
			.after($('.single-doctors .description>.container>.row:first-child>.col-9 .doctors__sidebar'));
		$('.single-clinics .description .col-9').after($('.single-clinics .description .col-9 .sidebar'));
	}
	$(window).resize(moveElements);
	function moveElements(){
		var width = document.documentElement.clientWidth;
		var height = document.documentElement.clientHeight;
		if (width <= 982) {
			$('.single-doctors .description__doctor').after($('.single-doctors .doctors__sidebar'));
			$('.single-clinics .description .description__rating-stars').after($('.single-clinics .description .sidebar'));
		}
		else{
			$('.single-doctors .description>.container>.row:first-child>.col-9')
				.after($('.single-doctors .description>.container>.row:first-child>.col-9 .doctors__sidebar'));
			$('.single-clinics .description .col-9').after($('.single-clinics .description .col-9 .sidebar'));
		}
	}

	//toggle tab after loading
	if(window.location.hash == '#clinics'){
		$( "header.all-doctors__header .link-2" ).trigger( "click" );
	}


	$(document).on('click', '.sidebar .content__map-btn', function(event) {
		event.preventDefault();
		$('.sidebar .content__map-block').toggleClass('content__map-block--active');
	});


	//show more info
	$(document).on('click', '.descr a.descr__more', function(event) {
		event.preventDefault();
		$(this).hide();
		$('.descr .descr__more-text').slideToggle();
	});


	//fix h1 top value for mobile device
	var width = document.documentElement.clientWidth;
	if (width <= 768) {
		var bredcrumbsHeight = $('.single-doctors .main .breadcrumbs').height();
		$('.h1--body-single-doctor').css('top', 100 + bredcrumbsHeight + 'px');
	}
	//h1 change padding before description (doctor single page)
	var titleHeightDoctor = $('.h1--body-single-doctor').height();
	$('.single-doctors .main .description .doctor .info__name').css('height', titleHeightDoctor + 'px');
	//h1 change padding before description (clinic single page)
	var titleHeightClinic = $('.h1--body-single-clinic').height();
	$('.single-clinics .main .description .clinic .info__name').css('height', titleHeightClinic + 'px');	
	$(window).resize(function() {
		//h1 change padding before description (doctor single page)
		var titleHeightDoctor = $('.h1--body-single-doctor').height();
		$('.single-doctors .main .description .doctor .info__name').css('height', titleHeightDoctor + 'px');
		//h1 change padding before description (clinic single page)
		var titleHeightClinic = $('.h1--body-single-clinic').height();
		$('.single-clinics .main .description .clinic .info__name').css('height', titleHeightClinic + 'px');	
		var width = document.documentElement.clientWidth;
		if (width <= 768) {
			var bredcrumbsHeight = $('.single-doctors .main .breadcrumbs').height();
			$('.h1--body-single-doctor').css('top', 100 + bredcrumbsHeight + 'px');
		}
	});

});

//mobile menu
$(document).on('click', 'a.mobile__menu.menu, .mobile-menu .menu__close', function(event) {
	event.preventDefault();
	$('.mobile-menu').toggleClass('mobile-menu--active');
});

//maps init
function initMap() {
	var map, map2;
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	map2 = new google.maps.Map(document.getElementById('map2'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	var address = {lat: 55.7675123, lng: 37.588919};
	var marker = new google.maps.Marker({
		map: map, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});

	var marker2 = new google.maps.Marker({
		map: map2, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker2.addListener('click', function() {
		infowindow.open(map2, marker2);
	});
}

function initMap2() {
	var map2;

	map2 = new google.maps.Map(document.getElementById('map2'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	var address = {lat: 55.7675123, lng: 37.588919};

	var marker2 = new google.maps.Marker({
		map: map2, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker2.addListener('click', function() {
		infowindow.open(map2, marker2);
	});
}

function initMap3() {
	var map2;

	map2 = new google.maps.Map(document.getElementById('map2'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	var address = {lat: 55.7675123, lng: 37.588919};

	var marker2 = new google.maps.Marker({
		map: map2, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker2.addListener('click', function() {
		infowindow.open(map2, marker2);
	});

	var map3;

	map3 = new google.maps.Map(document.getElementById('map3'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	var marker3 = new google.maps.Marker({
		map: map3, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker3.addListener('click', function() {
		infowindow.open(map3, marker3);
	});

	var map4;

	map4 = new google.maps.Map(document.getElementById('map4'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	var marker4 = new google.maps.Marker({
		map: map4, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker4.addListener('click', function() {
		infowindow.open(map4, marker4);
	});


	var map4;

	map5 = new google.maps.Map(document.getElementById('map5'), {
		center: {lat: 55.7675123, lng: 37.588919},
		zoom: 15
	});

	var marker5 = new google.maps.Marker({
		map: map5, 
		position: address, 
		title: 'Москва',
		icon: 'assets/images/map-placeholder.svg'
	});

	marker5.addListener('click', function() {
		infowindow.open(map5, marker5);
	});
}